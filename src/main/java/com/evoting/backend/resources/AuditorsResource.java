package com.evoting.backend.resources;

import com.evoting.backend.model.Auditor;
import com.evoting.backend.services.AuditorService;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@EnableAutoConfiguration
//@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RequestMapping("/auditors")
public class AuditorsResource {

    AuditorService au = new AuditorService();

    @CrossOrigin
    @RequestMapping(value = "/notifyRole", method = RequestMethod.POST)
    ArrayList< Auditor > createPasswords(@RequestBody  ArrayList<Auditor> listAuditors) {
        for (int counter = 0; counter < listAuditors.size(); counter++) {
            System.out.println(listAuditors.get(counter).getDni());
        }
        return au.createPasswords(listAuditors);
    }

}


