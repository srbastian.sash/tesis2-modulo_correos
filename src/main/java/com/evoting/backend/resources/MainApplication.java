package com.evoting.backend.resources;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;


@SpringBootApplication
public class MainApplication {

	public static void main(String[] args) throws Exception {
		//SpringApplication.run(ElectorResource.class, args);
		SpringApplication.run(AuditorsResource.class, args);
	}
}
