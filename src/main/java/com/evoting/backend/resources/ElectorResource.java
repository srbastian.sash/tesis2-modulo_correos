package com.evoting.backend.resources;

import com.evoting.backend.model.Elector;
import com.evoting.backend.model.Email;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import com.evoting.backend.services.ElectorService;
import com.evoting.backend.resources.WebConfig;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;

@RestController
@EnableAutoConfiguration
//@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@RequestMapping("/electors")
public class ElectorResource {

    ElectorService es = new ElectorService();

    @CrossOrigin
    @RequestMapping(value = "/generatePasswords", method = RequestMethod.POST)
    ArrayList< Elector > createPasswords(@RequestBody  ArrayList< Elector > listElectors) {
        for (int counter = 0; counter < listElectors.size(); counter++) {
            System.out.println(listElectors.get(counter).getDni());
        }

        //return listElectors;
        return es.createPasswords(listElectors);
    }

    @CrossOrigin
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
    int sendEmail(@RequestBody Email email ) throws IOException, MessagingException {
        return es.sendEmail(email.getElector(),email.getTextMessage(), email.getSubject());
    }

    @CrossOrigin
    @RequestMapping(value = "/sayHello", method = RequestMethod.GET)
    String sayHello() {
        return "Hello world!";
    }
}
