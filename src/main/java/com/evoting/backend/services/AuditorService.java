package com.evoting.backend.services;

import com.evoting.backend.model.Auditor;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

public class AuditorService {
    public AuditorService () {}

    public String generatePassword() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        System.out.println(generatedString);
        return generatedString;
    }

    public ArrayList<Auditor> createPasswords (ArrayList<Auditor> listAuditors){
        try{
            for (int counter = 0; counter < listAuditors.size(); counter++) {
                //Generate password and username
                listAuditors.get(counter).setPassword(generatePassword());
                System.out.println(listAuditors.get(counter));
            }
            //Send email with credentials
            new Thread(() -> {
                try {
                    for (int counter = 0; counter < listAuditors.size(); counter++) {
                        String messageText = "Estimado auditor"
                                + "\n\n Se le hace presente la generación de su contraseña para poder ingresar a la plataforma virtual de voto electrónico."
                                + "\n\n      contraseña: " + listAuditors.get(counter).getPassword();
                        String messageSubject = "Proceso electoral - Permisos asignados para Auditor";

                        sendEmail(listAuditors.get(counter), messageText, messageSubject);
                    }
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }).start();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return listAuditors;
    }

    public int sendEmail (Auditor elector, String messageText, String messageSubject) throws AddressException, MessagingException, IOException {
        int result = 0;
        try{
            //It works
            final String username = "tesis.evoting2020@gmail.com";
            final String password = "Andres2016";

            Properties prop = new Properties();
            prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.port", "587");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true"); //TLS

            Session session = Session.getInstance(prop,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("tesis.evoting2020@gmail.com"));
                message.setRecipients(
                        Message.RecipientType.TO,
                        InternetAddress.parse(elector.getEmail())
                );
                message.setSubject(messageSubject);
                message.setText(messageText);

                Transport.send(message);

                System.out.println("Done");
                result = 1;

            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

}
