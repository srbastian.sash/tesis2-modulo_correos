package com.evoting.backend.services;

import com.evoting.backend.model.Elector;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class ElectorService {
    public ElectorService () {}

    public String generatePassword() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        System.out.println(generatedString);
        return generatedString;
    }

    public ArrayList<Elector> createPasswords (ArrayList<Elector> listElectors){
        try{
            for (int counter = 0; counter < listElectors.size(); counter++) {
                //Generate password and username
                listElectors.get(counter).setPassword(generatePassword());
                System.out.println(listElectors.get(counter));
            }
            //Send email with credentials
            new Thread(() -> {
                try {
                    for (int counter = 0; counter < listElectors.size(); counter++) {
                        String messageText = "Estimado " + listElectors.get(counter).getName() + " " + listElectors.get(counter).getLastName() + ','
                                + "\n\n Se le hace presente la generación de sus contraseña para poder ingresar a la plataforma virtual de voto electrónico. Así mismo se le envía su clave privada para que pueda firmar su voto."
                                + "\n\n      contraseña: " + listElectors.get(counter).getPassword()
                                +  "\n      clave privada: " + listElectors.get(counter).getPrivateKey()
                                +  "\n      clave pública: " + listElectors.get(counter).getPublicKey();
                        String messageSubject = "Proceso electoral - Credenciales Creadas";

                        sendEmail(listElectors.get(counter), messageText, messageSubject);
                    }
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }).start();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return listElectors;
    }

    public int sendEmail (Elector elector, String messageText, String messageSubject) throws AddressException, MessagingException, IOException {
        int result = 0;
        try{
            //It works
            final String username = "tesis.evoting2020@gmail.com";
            final String password = "Andres2016";

            Properties prop = new Properties();
            prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.port", "587");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.starttls.enable", "true"); //TLS

            Session session = Session.getInstance(prop,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
            });

            try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("tesis.evoting2020@gmail.com"));
                message.setRecipients(
                        Message.RecipientType.TO,
                        InternetAddress.parse(elector.getEmail())
                );
                message.setSubject(messageSubject);
                message.setText(messageText);

                Transport.send(message);

                System.out.println("Done");
                result = 1;

            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

}
