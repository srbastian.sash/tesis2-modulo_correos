package com.evoting.backend.model;

public class Auditor  {
    private int id_person;
    private String email;
    private String dni;
    private String publicKey;
    private String password;
    public Auditor() {
    }

    public Auditor(Elector p) {
        if(p == null) return;
    }
    public Auditor(  String email, String DNI, String publicKey ) {
        this.email = email;
        this.dni = dni;
        this.publicKey = publicKey;
    }

    /**
     * @param id_person the id to set
     */
    public void setId_person(int id_person) {
        this.id_person = id_person;
    }

    /**
     * @return the id
     */
    public int getId_person() {
        return id_person;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }


    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the DNI to set
     */
    public void setDNI(String dni) {
        this.dni = dni;
    }

    /**
     * @return the publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
