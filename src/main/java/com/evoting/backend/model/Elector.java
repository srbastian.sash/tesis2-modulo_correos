package com.evoting.backend.model;

public class Elector  {
    private int id_person;
    private String name;
    private String lastName;
    private String email;
    private String dni;
    private String publicKey;
    private String privateKey;
    private String password;
    private boolean castVote;
    private int index;
    public Elector() {
    }

    public Elector(Elector p) {
        if(p == null) return;
        this.name = p.name;
        this.lastName = p.lastName;
    }
    public Elector( String first_name, String lastName, String email, String DNI, String publicKey, String privateKey ) {
        this.name = first_name;
        this.lastName = lastName;
        this.email = email;
        this.dni = dni;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    /**
     * @param id_person the id to set
     */
    public void setId_person(int id_person) {
        this.id_person = id_person;
    }

    /**
     * @return the id
     */
    public int getId_person() {
        return id_person;
    }

    /**
     * @return the first_name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the first_name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the DNI to set
     */
    public void setDNI(String dni) {
        this.dni = dni;
    }

    /**
     * @return the publicKey
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * @param publicKey the publicKey to set
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * @return the privateKey
     */
    public String getPrivateKey() {
        return privateKey;
    }

    /**
     * @param privateKey the privateKey to set
     */
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

}
